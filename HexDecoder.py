#!/usr/bin/env python
#coding: utf-8

import os
import string
import re
import sys

STRINGLENGTH=6 #Longueur minimum des chaînes à trouver
SPECIALCHAR=" ./#_"


##### Trouve les chaînes de taille supérieure ou égale à STRINGLENGTH #####
def findStrings(text):
    i,j=0,0
    trimmedData=[]


    if(text[i]=='\x00'):
        i+=1

    while(i < len(text)-STRINGLENGTH):
        j=i
        while(j < len(text)-STRINGLENGTH and text[j]!='\x00'):
            j+=1
        if(j-i>=STRINGLENGTH):
            trimmedData.append("".join(text[i:j]))
        i=j+1

    return trimmedData


def getString(index, text):
    i=index
    found
    while(i!='\x00'):
        i-=1



i=0
j=0
dataLength=0
pattern=[] #Liste des motifs recherchés
topics=[] #Liste des topcis
ip="" #Adresse IP

hexFile = open("hexFile.txt", 'r')
hexOut = open("hexOut", 'w')

content = hexFile.read() #Contenu du .hex
dataContent = [] #Données du .hex mises bout à bout

##### Conversion et affichage du fichier .hex #####

while(i <len(content)-1):

   dataLength = int(content[i+1:i+3],16) #Longueur des données
   data = content[i+9:i+9+2*dataLength] #Données d'une ligne

   for j in range(0, len(data), 2): #Lecture du contenu de data, octet par octet
       char=chr(int(data[j]+data[j+1], 16)) #Conversion de l'octet en caractère
       if ((char in string.ascii_letters) or (char in string.digits) or (char in SPECIALCHAR)):
           dataContent.append(char)
       elif (char=='\x00' and dataContent[-1]!='\x00'):
           dataContent.append('\x00')


    ##### Ecriture dans un fichier de sortie (facultatif) #####
   #for j in range(0, len(data), 2): #Parcours du fichier .hex
    #char=chr(int(data[j]+data[j+1], 16))
    #if ((char in string.ascii_letters) or (char in string.digits) or (char in " _#./")):
    #    hexOut.write(char)
    #else:
    #    hexOut.write("0x"+data[j]+data[j+1])
   #hexOut.write('\n')

   #Passage à la ligne suivante
   while(content[i]!='\n'):
    i+=1
   i+=1

 ##### Recherche de données en particulier #####
trimmedData = findStrings(dataContent)

for j in trimmedData:
    print(j);


#Recherche de motif
if(len(sys.argv)>1): #Si des arguments ont été précisés...
    pattern = sys.argv[1:len(sys.argv)]
else: #Sinon on demande à l'utilisateur
    pattern = raw_input("\n\nPattern to search : ").split(" ")

print("\n\n##########SEARCH RESULTS##########\n")

for s in trimmedData:
    if(ip==""): #Si l'IP n'a pas encore été trouvé...
        ipTemp=(re.findall(r'[0-9]+(?:\.[0-9]+){3}', s)) #Recherche de l'@ IP
        if(ipTemp!=[]): #Si une IP est trouvée, elle est sauvegardée
            ip=s
    for p in range(0, len(pattern)):
        if(pattern[p] in s):
            print(s)

print("\n@IP : "+ip)





hexFile.close()
hexOut.close()
